module gitlab.com/sunaryaagung/golang-messenger-api

go 1.13

require (
	cloud.google.com/go v0.46.3
	cloud.google.com/go/storage v1.4.0
	github.com/GoogleCloudPlatform/cloudsql-proxy v0.0.0-20191122183716-9dc2dde4f249
	github.com/badoux/checkmail v0.0.0-20181210160741-9661bd69e9ad
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.3
	github.com/jinzhu/gorm v1.9.11
	github.com/joho/godotenv v1.3.0
	golang.org/x/crypto v0.0.0-20191128160524-b544559bb6d1
)
