package main

import (
	"fmt"
	"log"
	"os"

	"github.com/joho/godotenv"
	"gitlab.com/sunaryaagung/golang-messenger-api/controllers"
)

var server = controllers.Server{}

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		loadEnv()
		port = "8080"
	}

	server.ConnectDB(
		os.Getenv("DB_HOST"),
		os.Getenv("DB_NAME"),
		os.Getenv("DB_USER"),
		os.Getenv("DB_PASSWORD"),
	)
	log.Printf("Listening on localhost:%s", port)
	server.Serve(fmt.Sprintf(":%s", port))
}

func loadEnv() {
	var err error
	err = godotenv.Load()
	if err != nil {
		log.Fatalf("Can't load env:%s", err)
	}
	fmt.Println("Get env values")
}
