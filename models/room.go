package models

import (
	"errors"
	"time"

	"github.com/jinzhu/gorm"
)

// Room model
type Room struct {
	ID        int       `gorm:"primary_key;auto_increment;" json:"id"`
	CreatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
	AdminID   int       `gorm:"not null;" json:"admin_id"`
	Admin     User      `json:"admin"`
	RoomName  string    `gorm:"size:100;not null;unique_index" json:"room_name"`
}

type roomMember struct {
	Name     string
	Room     int
	RoomName string
}

// Prepare user model
func (r *Room) Prepare() {
	r.ID = 0
	r.CreatedAt = time.Now()
	r.UpdatedAt = time.Now()
}

// CreateRoom func
func (r *Room) CreateRoom(db *gorm.DB) (*Room, error) {
	var err error
	err = db.Debug().Model(&Room{}).Create(&r).Error
	if err != nil {
		return &Room{}, err
	}
	if r.ID != 0 {
		err = db.Debug().Model(&User{}).Where("id = ?", r.AdminID).Take(&r.Admin).Error
		if gorm.IsRecordNotFoundError(err) {
			db.Delete(&r)
			return &Room{}, err
		}
	}

	return r, nil
}

// GetAllRoom func
func (r *Room) GetAllRoom(db *gorm.DB, uid int) (*[]Room, error) {
	var err error
	rooms := []Room{}
	err = db.Debug().Model(&Room{}).Joins("JOIN members ON rooms.id = members.room_id").Joins("JOIN users ON members.user_id = users.id").Where("users.id = ?", uid).Find(&rooms).Error
	if err != nil {
		panic(err)
	}
	return &rooms, nil
}

// GetOneRoom func
func (r *Room) GetOneRoom(db *gorm.DB, rid int) (*Member, error) {
	var err error
	roomMember := Member{}
	err = db.Debug().Model(&Member{}).Where("room_id= ?", rid).Take(&roomMember).Error
	if err != nil {
		return &Member{}, err
	}
	if gorm.IsRecordNotFoundError(err) {
		return &Member{}, errors.New("Not found")
	}
	return &roomMember, err
}

// DeleteRoom func
func (r *Room) DeleteRoom(db *gorm.DB, rid int) (int64, error) {

	db = db.Debug().Model(&Member{}).Delete(&Member{}, "room_id = ?", rid)
	if db.Error != nil {
		return 0, db.Error
	}

	db = db.Debug().Model(&Message{}).Delete(&Message{}, "room_id = ?", rid)
	if db.Error != nil {
		return 0, db.Error
	}

	db = db.Debug().Model(&Room{}).Delete(&Room{}, "id = ?", rid)
	if db.Error != nil {
		return 0, db.Error
	}

	return db.RowsAffected, nil
}
