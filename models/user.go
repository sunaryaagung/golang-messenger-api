package models

import (
	"errors"
	"html"
	"strings"
	"time"

	"github.com/badoux/checkmail"
	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

//User model
type User struct {
	ID          int       `gorm:"primary_key;auto_increment;" json:"id"`
	CreatedAt   time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt   time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
	Name        string    `gorm:"size:100;not null;" json:"name"`
	Email       string    `gorm:"size:100;not null;unique_index" json:"email"`
	Password    string    `gorm:"size:100;not null" json:"password"`
	ProfilePict string    `gorm:"size:100;default:'picture'" json:"profile_picture"`
}

// Prepare user model
func (u *User) Prepare() {
	u.ID = 0
	u.Name = html.EscapeString(strings.TrimSpace(u.Name))
	u.Email = html.EscapeString(strings.TrimSpace(u.Email))
	u.CreatedAt = time.Now()
	u.UpdatedAt = time.Now()
}

// Hash user password
func Hash(password string) ([]byte, error) {
	return bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
}

// BeforeSave func on gorm
func (u *User) BeforeSave() error {
	hashedPassword, err := Hash(u.Password)
	if err != nil {
		return err
	}
	u.Password = string(hashedPassword)
	return nil
}

// CheckPassword func
func CheckPassword(hashedPassword, password string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
}

// Validate user cation
func (u *User) Validate(action string) error {
	switch strings.ToLower(action) {
	case "login":
		if u.Email == "" {
			return errors.New("Email is required")
		}
		if u.Password == "" {
			return errors.New("Password is required")
		}
		return nil
	default:
		if u.Name == "" {
			return errors.New("Name is required")
		}
		if u.Email == "" {
			return errors.New("Email is required")
		}
		if err := checkmail.ValidateFormat(u.Email); err != nil {
			return errors.New("Invalid email format")
		}
		if u.Password == "" {
			return errors.New("Password is required")
		}
		return nil
	}
}

//AddUser func
func (u *User) AddUser(db *gorm.DB) (*User, error) {
	var err error
	err = db.Debug().Create(&u).Error
	if err != nil {
		return &User{}, err
	}
	return u, nil

}

//GetAllUsers function
func (u *User) GetAllUsers(db *gorm.DB) (*[]User, error) {
	var err error
	users := []User{}
	err = db.Debug().Model(&User{}).Find(&users).Error
	if err != nil {
		return &[]User{}, err
	}
	return &users, err
}

//GetOneUser function
func (u *User) GetOneUser(db *gorm.DB, uid int) (*User, error) {
	var err error
	err = db.Debug().Model(&User{}).Where("id = ?", uid).Take(&u).Error
	if err != nil {
		return &User{}, err
	}
	if gorm.IsRecordNotFoundError(err) {
		return &User{}, errors.New("Not found")
	}
	return u, err
}

//DeteleUser func
func (u *User) DeteleUser(db *gorm.DB, uid int) (int64, error) {
	db = db.Debug().Model(&User{}).Where("id = ?", uid).Take(&User{}).Unscoped().Delete(&User{})
	if db.Error != nil {
		return 0, db.Error
	}
	return db.RowsAffected, nil
}

//UpdateUser func
func (u *User) UpdateUser(db *gorm.DB, uid int) (*User, error) {
	err := u.BeforeSave()
	if err != nil {
		return &User{}, err
	}
	db = db.Debug().Model(&User{}).Where("id = ?", uid).Take(&User{}).UpdateColumns(
		map[string]interface{}{
			"name":     u.Name,
			"email":    u.Email,
			"password": u.Password,
		},
	)
	if db.Error != nil {
		return &User{}, err
	}
	//display updated data
	err = db.Debug().Model(&User{}).Where("id = ?", uid).Take(&u).Error
	if err != nil {
		return &User{}, err
	}
	return u, nil
}

//AddProfilePict func
func (u *User) AddProfilePict(db *gorm.DB, uid int, pictURL string) (*User, error) {
	var err error
	db = db.Debug().Model(&User{}).Select("profile_pict").Where("id = ?", uid).Updates(map[string]interface{}{"profile_pict": pictURL})
	if db.Error != nil {
		return &User{}, err
	}
	err = db.Debug().Model(&User{}).Select("*").Take(&u).Error
	if err != nil {
		return &User{}, err
	}
	return u, nil
}
