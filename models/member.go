package models

import (
	"errors"
	"time"

	"github.com/jinzhu/gorm"
)

// Member model
type Member struct {
	ID        int       `gorm:"primary_key;auto_increment;" json:"id"`
	CreatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
	RoomID    int       `gorm:"not null;" json:"room_id"`
	UserID    int       `gorm:"not null;" json:"user_id"`
	User      User      `json:"user"`
}

// Prepare user model
func (m *Member) Prepare() {
	m.ID = 0
	m.User = User{}
	m.CreatedAt = time.Now()
	m.UpdatedAt = time.Now()
}

// AddMember to room
func (m *Member) AddMember(db *gorm.DB) (*Member, error) {
	var err error
	err = db.Debug().Create(&m).Error
	if err != nil {
		return &Member{}, err
	}
	if m.ID != 0 {
		err = db.Debug().Model(&User{}).Where("id = ?", m.UserID).Take(&m.User).Error
		if gorm.IsRecordNotFoundError(err) {
			db.Delete(&m)
			return &Member{}, err
		}
	}
	return m, nil
}

// AddAdmin func
func (m *Member) AddAdmin(db *gorm.DB, adminID, roomID int) (*Member, error) {
	var err error
	m.RoomID = roomID
	m.UserID = adminID
	err = db.Debug().Create(&m).Error
	if err != nil {
		return &Member{}, err
	}
	if m.ID != 0 {
		err = db.Debug().Model(&User{}).Where("id = ?", m.UserID).Take(&m.User).Error
		if gorm.IsRecordNotFoundError(err) {
			db.Delete(&m)
			return &Member{}, err
		}
	}
	return m, nil
}

// GetMember in room
func (m *Member) GetMember(db *gorm.DB, uid int) (*[]Member, error) {
	var err error
	members := []Member{}
	err = db.Debug().Model(&Member{}).Where("room_id = ?", uid).Find(&members).Error
	if err != nil {
		return &[]Member{}, err
	}
	if len(members) > 0 {
		for i := range members {
			err := db.Debug().Model(&User{}).Where("id = ? ", members[i].UserID).Take(&members[i].User).Error
			if err != nil {
				return &[]Member{}, err
			}
		}
	}
	return &members, err
}

//CheckDuplicate func
func (m *Member) CheckDuplicate(db *gorm.DB, rid, uid int) (err error) {
	members := []Member{}
	err = db.Debug().Model(&Member{}).Where("user_id = ? AND room_id = ?", uid, rid).Find(&members).Error
	if err != nil {
		return err
	}
	if len(members) > 0 {
		return errors.New("Already registered")
	}
	return nil

}
