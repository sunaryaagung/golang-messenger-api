package models

import (
	"time"

	"github.com/jinzhu/gorm"
)

// Message model
type Message struct {
	ID        int       `gorm:"primary_key;auto_increment;" json:"id"`
	CreatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
	SenderID  int       `gorm:"not null;" json:"sender_id"`
	Sender    User      `json:"sender"`
	RoomID    int       `gormL:"not null;" json:"room_id"`
	Content   string    `gorm:"size:300;" json:"content"`
}

// Prepare user model
func (msg *Message) Prepare() {
	msg.ID = 0
	msg.Sender = User{}
	msg.CreatedAt = time.Now()
	msg.UpdatedAt = time.Now()
}

// CreateMessage func
func (msg *Message) CreateMessage(db *gorm.DB) (*Message, error) {
	var err error
	err = db.Debug().Create(&msg).Error
	if err != nil {
		return &Message{}, err
	}
	if msg.ID != 0 {
		err = db.Debug().Model(&User{}).Where("id = ?", msg.SenderID).Take(&msg.Sender).Error
		if err != nil {
			return &Message{}, err
		}
	}
	return msg, nil
}

// GetMessage on room func
func (msg *Message) GetMessage(db *gorm.DB, rid int) (*[]Message, error) {
	var err error
	messages := []Message{}
	err = db.Debug().Model(&Message{}).Where("room_id = ?", rid).Find(&messages).Error
	if err != nil {
		return &[]Message{}, err
	}
	if len(messages) > 0 {
		for i := range messages {
			err := db.Debug().Model(&User{}).Where("id = ? ", messages[i].SenderID).Take(&messages[i].Sender).Error
			if err != nil {
				return &[]Message{}, err
			}
		}
	}
	return &messages, err
}

// DeleteMessage func
func (msg *Message) DeleteMessage(db *gorm.DB, mid int) (int64, error) {
	db = db.Debug().Model(&Message{}).Where("id = ?", mid).Take(&Message{}).Unscoped().Delete(&Message{})
	if db.Error != nil {
		return 0, db.Error
	}
	return db.RowsAffected, nil
}
