package controllers

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"

	_ "github.com/jinzhu/gorm/dialects/postgres" //postgres database driver
	"gitlab.com/sunaryaagung/golang-messenger-api/models"
)

//Server model
type Server struct {
	DB     *gorm.DB
	Router *mux.Router
}

//ConnectDB func
func (server *Server) ConnectDB(dbHost, dbName, dbUser, dbPassword string) {
	var err error

	dsn := fmt.Sprintf("host=%s dbname=%s user=%s password=%s sslmode=disable", dbHost, dbName, dbUser, dbPassword)

	server.DB, err = gorm.Open("postgres", dsn)
	if err != nil {
		fmt.Printf("%s", dbName)
		log.Fatal("Error:", err)
	}
	fmt.Println("Connected to db:", dbName)
	server.DB.Debug().AutoMigrate(models.User{}, models.Room{}, models.Member{}, models.Message{})
	server.Router = mux.NewRouter()
	server.RunRouters()
}

//Serve func
func (server *Server) Serve(addr string) {
	headers := handlers.AllowedHeaders([]string{"*"})
	methods := handlers.AllowedMethods([]string{"GET", "POST", "PUT", "DELETE"})
	origins := handlers.AllowedOrigins([]string{"*"})
	log.Fatal(http.ListenAndServe(":8080", handlers.CORS(headers, methods, origins)(server.Router)))
}
