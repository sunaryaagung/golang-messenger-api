package controllers

import (
	"context"
	"crypto/sha1"
	"errors"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"strings"
	"time"

	"cloud.google.com/go/storage"
	"gitlab.com/sunaryaagung/golang-messenger-api/auth"
	"gitlab.com/sunaryaagung/golang-messenger-api/models"
	"gitlab.com/sunaryaagung/golang-messenger-api/responses"
)

var gcsBucket = "ginkgo-ai-my-bucket"

// AddProfilePicture handler
func (server *Server) AddProfilePicture(w http.ResponseWriter, r *http.Request) {
	uid, err := auth.ExtractTokenID(r)
	if err != nil {
		responses.ERROR(w, http.StatusUnauthorized, err)
		return
	}

	multiPartFile, headerFile, err := r.FormFile("file")
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	defer multiPartFile.Close()
	fileName, err := uploadFile(r, multiPartFile, headerFile)
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	fileURL := "https://storage.cloud.google.com/" + gcsBucket + `/` + fileName
	user := models.User{}
	user.UpdatedAt = time.Now()
	updatedUser, err := user.AddProfilePict(server.DB, uid, fileURL)
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}

	responses.JSON(w, http.StatusOK, updatedUser)

}

func uploadFile(r *http.Request, multiPartFile multipart.File, headerFile *multipart.FileHeader) (string, error) {
	ext, err := fileFilter(r, headerFile)
	if err != nil {
		return "", err
	}
	name := getSha(multiPartFile) + `.` + ext
	multiPartFile.Seek(0, 0)
	ctx := context.Background()
	return name, putFile(ctx, name, multiPartFile)

}

func fileFilter(r *http.Request, headerFile *multipart.FileHeader) (string, error) {
	ext := headerFile.Filename[strings.LastIndex(headerFile.Filename, ".")+1:]

	switch strings.ToLower(ext) {
	case "jpg", "jpeg", "png":
		return ext, nil
	}
	return ext, errors.New("Not supported type")
}

func getSha(src multipart.File) string {
	h := sha1.New()
	io.Copy(h, src)
	return fmt.Sprintf("%x", h.Sum(nil))
}

func putFile(ctx context.Context, name string, rdr io.Reader) error {
	client, err := storage.NewClient(ctx)
	if err != nil {
		return err
	}
	defer client.Close()

	writer := client.Bucket(gcsBucket).Object(name).NewWriter(ctx)

	io.Copy(writer, rdr)
	return writer.Close()
}
