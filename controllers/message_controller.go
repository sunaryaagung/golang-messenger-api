package controllers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/sunaryaagung/golang-messenger-api/auth"
	"gitlab.com/sunaryaagung/golang-messenger-api/models"
	"gitlab.com/sunaryaagung/golang-messenger-api/responses"
)

// CreateMessage func
func (server *Server) CreateMessage(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	message := models.Message{}
	err = json.Unmarshal(body, &message)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	senderID, err := auth.ExtractTokenID(r)
	if err != nil {
		responses.ERROR(w, http.StatusUnauthorized, err)
		return
	}
	message.SenderID = senderID

	message.Prepare()
	messageCreated, err := message.CreateMessage(server.DB)
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	w.Header().Set("Location", fmt.Sprintf("%s%s/%d", r.Host, r.RequestURI, messageCreated.ID))
	responses.JSON(w, http.StatusOK, messageCreated)
}

// GetMessage on room func
func (server *Server) GetMessage(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	rid, err := strconv.Atoi(vars["id"])
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	message := models.Message{}
	messages, err := message.GetMessage(server.DB, rid)
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	responses.JSON(w, http.StatusOK, messages)
}

// DeleteMessage func
func (server *Server) DeleteMessage(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	mid, err := strconv.Atoi(vars["id"])
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	message := models.Message{}
	_, err = message.DeleteMessage(server.DB, mid)
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	w.Header().Set("Entity", fmt.Sprintf("%d", mid))
	responses.JSON(w, http.StatusNoContent, "")
}
