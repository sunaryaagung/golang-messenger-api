package controllers

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"gitlab.com/sunaryaagung/golang-messenger-api/auth"
	"gitlab.com/sunaryaagung/golang-messenger-api/models"
	"gitlab.com/sunaryaagung/golang-messenger-api/responses"
	"golang.org/x/crypto/bcrypt"
)

// CheckLogin func
func (server *Server) CheckLogin(email, password string) (string, error) {
	var err error
	user := models.User{}
	err = server.DB.Debug().Model(models.User{}).Where("email = ?", email).Take(&user).Error
	if err != nil {
		return "", err
	}

	err = models.CheckPassword(user.Password, password)
	if err != nil && err == bcrypt.ErrMismatchedHashAndPassword {
		return "", err
	}
	return auth.CreateToken(user.ID)
}

//Login func
func (server *Server) Login(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	user := models.User{}
	err = json.Unmarshal(body, &user)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	user.Validate("login")
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	token, err := server.CheckLogin(user.Email, user.Password)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	responses.JSON(w, http.StatusOK, token)
}
